Investogator
============

## Purpose
I made this when I wanted to get into investing in ETFs, but wasn't sure which ones were safe to pick
for each category (technology, large cap growth, health/biotech, etc). My dad (who's big on investing)
pointed me to a few sources for getting reviews on ETFs, but these didn't have a free API to query.
Thus, I ended up leveraging BeautifulSoup to get the data I wanted so I wouldn't have to spend hours
checking dozens of ETF symbols across multiple sites.

Update: Unfortunately ETFdb.com completely redid the way ETFs and their categories are organized so
right now this tool only lets you look up rankings by symbol instead of category. There doesn't seem
to be a sane way to get the data now that html scraping through paginated results is required

## What sites does it check?
#### For ratings:
- https://www.zacks.com 
- http://www.morningstar.com

#### For sustainability:
- http://etfs.morningstar.com

## Limitations
Right now you need to know the ETF symbol you want to look up. It would be nice to instead
be able to check by category (e.g. large-cap-blend) so you can discover other ETFs in that
category that might be a better choice.